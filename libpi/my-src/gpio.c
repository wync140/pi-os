#include "rpi.h"

// see broadcomm documents for magic addresses.
#define GPIO_BASE 0x20200000
volatile unsigned *gpio_fsel0     = (void*)(GPIO_BASE + 0x00);
volatile unsigned *gpio_set0      = (void*)(GPIO_BASE + 0x1C);
volatile unsigned *gpio_clr0      = (void*)(GPIO_BASE + 0x28);
volatile unsigned *gpio_lev0      = (void*)(GPIO_BASE + 0x34);
volatile uint32_t *gpio_gppud     = (void*)(GPIO_BASE + 0x94);
volatile uint32_t *gpio_gppudclk0 = (void*)(GPIO_BASE + 0x98);


// set GPIO function for <pin> (input, output, alt...).  settings for other
// pins should be unchanged.
void gpio_set_function(unsigned pin, gpio_func_t function) {
    if(pin >= 32) {
        return;
    }
    // max GPIO function allowed
    if( (unsigned)function > 7){
        return;
    }
    unsigned fsel_offset    = (pin / 10);
    unsigned pin_group_num  =  pin % 10;
    unsigned writeback = get32(&gpio_fsel0[fsel_offset]);
    dsb();
    writeback &= ~(0b111 << (pin_group_num * 3));
    writeback |=  ((unsigned)function << (pin_group_num * 3));
    put32(&gpio_fsel0[fsel_offset], writeback);
}


void gpio_set_output(unsigned pin) {
    if(pin >= 32) {
        return;
    }
    unsigned fsel_offset    = (pin / 10);
    unsigned pin_group_num  =  pin % 10;
    unsigned writeback = get32(&gpio_fsel0[fsel_offset]);
    dsb();
    writeback &= ~(0b111 << (pin_group_num * 3));
    writeback |=  (0b001 << (pin_group_num * 3));
    put32(&gpio_fsel0[fsel_offset], writeback);
}

// set GPIO <pin> on.
void gpio_set_on(unsigned pin) {
    if(pin >= 32) {
        return;
    }
    dsb();
    put32(&gpio_set0[pin/32],(0b1 << (pin % 32) ));
}

// set GPIO <pin> off
void gpio_set_off(unsigned pin) {
    if(pin >= 32) {
        return;
    }
    dsb();
    put32(&gpio_clr0[pin/32],(0b1 << (pin % 32) ));
}


void gpio_set_input(unsigned pin) {
    if(pin >= 32) {
        return;
    }
    unsigned fsel_offset    = (pin / 10);
    unsigned pin_group_num  =  pin % 10;
    unsigned writeback = get32(&gpio_fsel0[fsel_offset]);
    dsb();
    writeback &= ~(0b111 << (pin_group_num * 3));
    writeback |=  (0b000 << (pin_group_num * 3));
    put32(&gpio_fsel0[fsel_offset], writeback);
}

// return the value of <pin>
int gpio_read(unsigned pin) {
    if(pin >= 32) {
        return 0;
    }
    unsigned v = 0;
    v = !!( get32(&gpio_lev0[pin/32]) & (0b1 << (pin % 32)) );
    dsb();
    return v;
}

// set <pin> to <v> (v \in {0,1})
void gpio_write(unsigned pin, unsigned v) {
    if(pin >= 32) {
        return;
    }
    if(v)
       gpio_set_on(pin);
    else
       gpio_set_off(pin);
}

// follow this https://forums.raspberrypi.com/viewtopic.php?t=189885
// and this https://github.com/dwelch67/raspberrypi/blob/master/uart01/uart01.c
static void gpio_set_pullupdown_config(unsigned pin, unsigned mode){
    dsb();
    put32(gpio_gppud,mode);
    delay_us(150);
    put32(gpio_gppudclk0,(1<<pin));
    delay_us(150);
    put32(gpio_gppudclk0,0);
    put32(gpio_gppud,0);
}


void gpio_set_pullup(unsigned pin){
    if(pin >= 32) {
        return;
    }
    // set to pullup
    gpio_set_pullupdown_config(pin, 0b10);
}

