// implement:
//  void uart_init(void)
//
//  int uart_can_getc(void);
//  int uart_getc(void);
//
//  int uart_can_putc(void);
//  void uart_putc(unsigned c);
//
// see that hello world works.
//
//
#include "rpi.h"

// called first to setup uart to 8n1 115200  baud,
// no interrupts.
//  - you will need memory barriers, use <dev_barrier()>
//
//  later: should add an init that takes a baud rate.

//((250,000,000/115200)/8)-1 = 270
#define BAUD_RATE_DIVISOR 270
#define UART_TX_PIN 14
#define UART_RX_PIN 15

#define ENABLE_UART_DEVICE 0b1

#define IER_DISABLE_INTERUPTS 0

#define CNTL_COMPLETE_DISABLE 0
#define CNTL_ENABLE_TX_RX 0b11

#define LCR_ENABLE_8_BIT_MODE 0b11

#define STAT_RCV_IDLE_BITMASK 0b100
#define STAT_TX_SPACE_AVAIL_BITMASK 0b10
#define STAT_RX_SYMBOL_AVAIL_BITMASK 0b1

#define IIR_CLEAR_FIFOS 0b110


volatile uint32_t* AUX_ENABLE = (uint32_t*)0x20215004;

typedef struct {
    uint32_t AUX_MU_IO_REG;
    uint32_t AUX_MU_IER_REG;
    uint32_t AUX_MU_IIR_REG;
    uint32_t AUX_MU_LCR_REG;
    uint32_t AUX_MU_MCR_REG;
    uint32_t AUX_MU_LSR_REG;
    uint32_t AUX_MU_MSR_REG;
    uint32_t AUX_MU_SCRATCH;
    uint32_t AUX_MU_CNTL_REG;
    uint32_t AUX_MU_STAT_REG;
    uint32_t AUX_MU_BAUD_REG;
} Mini_UART_t;

static volatile Mini_UART_t* mini_uart = (Mini_UART_t*)0x20215040;

void uart_init(void) {

    // set to pullup for line stability
    gpio_set_pullup(UART_TX_PIN);
    gpio_set_pullup(15);

    // enable before turning on UART per suggestion of datasheet
    // ALT 5 is UART 1 (mini uart
    gpio_set_function(UART_TX_PIN,GPIO_FUNC_ALT5);
    gpio_set_function(UART_RX_PIN,GPIO_FUNC_ALT5);
    

    // mem barrier, before first put
    dsb();

    // turn on uart
    uint32_t v = get32(AUX_ENABLE);
    put32(AUX_ENABLE, v | ENABLE_UART_DEVICE);

    // disable TX RX
    put32(&mini_uart->AUX_MU_CNTL_REG, CNTL_COMPLETE_DISABLE);

    // disable Interrupts (but may mess with baud rate)
    put32(&mini_uart->AUX_MU_IER_REG, IER_DISABLE_INTERUPTS);

    // wait for RX to go dark
    while( (get32(&mini_uart->AUX_MU_STAT_REG) & STAT_RCV_IDLE_BITMASK) == 0){
        dummy(0);
    }

    // reset DLAB and set bit mode (8bit)
    put32(&mini_uart->AUX_MU_LCR_REG, LCR_ENABLE_8_BIT_MODE);

    // clear MCR for good practice
    put32(&mini_uart->AUX_MU_MCR_REG, 0);

    // disable interrupts again in case DLAB was set during the above one
    put32(&mini_uart->AUX_MU_IER_REG, IER_DISABLE_INTERUPTS);

    // set baud rate
    put32(&mini_uart->AUX_MU_BAUD_REG, BAUD_RATE_DIVISOR);

    // flush FIFOs
    put32(&mini_uart->AUX_MU_IIR_REG, IIR_CLEAR_FIFOS);

    // ready to turn back on uart! yay
    put32(&mini_uart->AUX_MU_CNTL_REG, CNTL_ENABLE_TX_RX);

    // finish up with a mem barrier after last read
    dsb();
}

// 1 = at least one byte on rx queue, 0 otherwise
static int uart_can_getc(void) {
    int ret = (get32(&mini_uart->AUX_MU_STAT_REG) & STAT_RX_SYMBOL_AVAIL_BITMASK) ? 1: 0;
    // mem barrier
    dsb();
    return ret;
}

// returns one byte from the rx queue, if needed
// blocks until there is one.
int uart_getc(void) {
	while(!uart_can_getc()){
        dummy(0);
    }
    // now we know a byte is there, return it
    int ret = (int)(get32(&mini_uart->AUX_MU_IO_REG));
    // dsb after last read
    dsb();
    return ret;
}

// 1 = space to put at least one byte, 0 otherwise.
int uart_can_putc(void) {
    int ret = (get32(&mini_uart->AUX_MU_STAT_REG) & STAT_TX_SPACE_AVAIL_BITMASK) ? 1: 0;
    // barrier after last read
    dsb();
    return ret;
}

// put one byte on the tx qqueue, if needed, blocks
// until TX has space.
void uart_putc(unsigned c) {
    // check we arent trying to put more than a byte on
    unsigned byte = c & 0xFF;
    assert(byte == c);
    while(!uart_can_putc()){
        dummy(0);
    }
    // uart putc will always dsb() before our first read (here) for us
    put32(&mini_uart->AUX_MU_IO_REG,byte);
}


// simple wrapper routines useful later.

// a maybe-more intuitive name for clients.
int uart_has_data(void) {
    return uart_can_getc();
}

int uart_getc_async(void) { 
    if(!uart_has_data())
        return -1;
    return uart_getc();
}
