/*
 * Simple tracing memory implementation.  Used for cross-checking.
 * 
 * you will implement a simple fake memory that maps addresses to values.  
 * e.g., an array, linked list, hash table of (<addr>, <val>) tuples such 
 * that after you insert some (<addr>, <val>), you can lookup <val> using 
 * <addr> as a key.
 *
 * simplistic assumptions:
 *  - all loads/stores are 32 bits.
 *  - read returns value of last write, or random() if none.
 *  - load and store prints the addr,val
 * 
 * HINT: I woul suggest a fixed size array, that you do a simple linear scan 
 * to insert, and lookup.
 *
 * mildly formal-ish rules for fake memory:
 *  1. no duplicate entries.  
 *       if: 
 *           (<addr>, <val0>) \in memory
 *           (<addr>, <val1>) \in memory
 *      then <val0> == <val1>
 *  2. addresses are persistent: if you ever write <addr>,<val> to memory, 
 *     then <addr> stays there forever.
 *  3. values are persistent until the next write: if you ever write 
 *     (<addr>,<val> ) to memory, then until the next write, 
 *          if (<addr>, <v>) \in memory, then <v> == <val>.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "avl_tree.h"
#include "demand.h"
#include "rpi.h"
#include "pi-test.h"

/****************************************************************************
 *          DO NOT CHANGE THIS CODE (your changes go after it)
 *          DO NOT CHANGE THIS CODE (your changes go after it)
 *          DO NOT CHANGE THIS CODE (your changes go after it)
 *          DO NOT CHANGE THIS CODE (your changes go after it)
 *          DO NOT CHANGE THIS CODE (your changes go after it)
 */
#define MEM_SIZE 10000

typedef struct {
    const volatile void *addr;
    unsigned val;
} mem_t;

typedef struct {
    int op;
    mem_t v;
} log_ent_t;

static mem_t mk_mem(const volatile void *addr, unsigned val) {
    return (mem_t) { .addr = addr, .val = val };
}

// don't change routine so we can compare to everyone.
static void print_write(mem_t *m) {
    printf("\tTRACE:PUT32(%p)=0x%x\n", m->addr, m->val);
}
// don't change routine so we can compare to everyone.
static void print_read(mem_t *m) {
    printf("\tTRACE:GET32(%p)=0x%x\n", m->addr, m->val);
}

struct mem_t_tree {
    mem_t mem;
    struct avl_tree_node index_node;
};

// tree root
static struct avl_tree_node *root = NULL;

#define GET_MEMT(i) avl_tree_entry((i), struct mem_t_tree, index_node);
#define GET_ADDR(i) avl_tree_entry((i), struct mem_t_tree, index_node)->mem.addr;
static int _avl_cmp_memaddr_to_node(const void *mem_ptr,
                const struct avl_tree_node *nodeptr)
{
    const volatile void** addr = (const volatile void**) mem_ptr; // cast to our typical
    uintptr_t search_addr = (uintptr_t)(*addr);
    uintptr_t node_addr = (uintptr_t) GET_ADDR(nodeptr);
    if (search_addr < node_addr)
        return -1;
    else if (search_addr > node_addr)
        return 1;
    else
        return 0;
}

static int _avl_cmp_memt_nodes(const struct avl_tree_node *node1, const struct avl_tree_node *node2)
{
    uintptr_t n1 = (uintptr_t) GET_ADDR(node1);
    uintptr_t n2 = (uintptr_t) GET_ADDR(node2);
    if (n1 < n2)
        return -1;
    else if (n1 > n2)
        return 1;
    else
        return 0;
}

unsigned get32(const volatile void *addr) {
    struct avl_tree_node *result = avl_tree_lookup(root, &addr, _avl_cmp_memaddr_to_node);

    if (result != NULL){
        // found
        mem_t mem_res = avl_tree_entry(result, struct mem_t_tree, index_node)->mem;
        print_read(&mem_res);
        return(mem_res.val);
    }
    else{
        // else we insert random
        struct mem_t_tree *mem_node = malloc(sizeof(struct mem_t_tree));
        mem_node->mem = mk_mem(addr,fake_random());
        if(avl_tree_insert(&root, &mem_node->index_node, _avl_cmp_memt_nodes)){
            // should never reach here
            free(mem_node);
            panic("Should have never reached here!. The lookup failed or something broke");
        }
        print_read(&(mem_node->mem));
        return mem_node->mem.val;
    }
}

void put32(volatile void *addr, unsigned val) {

    // else we insert random
    // pre allocate incase we 
    struct mem_t_tree *mem_node = malloc(sizeof(struct mem_t_tree));
    // set addr and vall
    mem_node->mem = mk_mem(addr,val);

    struct avl_tree_node* search_node = avl_tree_insert(&root, &mem_node->index_node, _avl_cmp_memt_nodes);
    if(search_node){
        // free the one we didnt insert
        free(mem_node);
        // replace with the old new
        mem_node = GET_MEMT(search_node);
        mem_node->mem.val = val;
        assert(addr==mem_node->mem.addr);
    }

    // mem node will be the correct memory at this point
    print_write(&(mem_node->mem));

}

