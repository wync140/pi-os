#include <libunix.h>
#include <unistd.h>

#define CLOSE_FDS_MAX 256
#define STDERR_FD 3

void close_open_fds(void) {
  int i;
  for (i = STDERR_FD + 1; i != CLOSE_FDS_MAX; i++) {
    (void)close(i);
  }
}