// engler, cs140e.
#include <assert.h>
#include <fcntl.h>
#include <string.h>

#include "libunix.h"
#include "demand.h"

#define _SVID_SOURCE
#include <dirent.h>
static const char *ttyusb_prefixes[] = {
	"ttyUSB",	// linux
	"cu.SLAB_USB", // mac os
	0
};


static int filter(const struct dirent *d) {
    // scan through the prefixes, returning 1 when you find a match.
    // 0 if there is no match.
    int i = 0;
    while(ttyusb_prefixes[i] != 0)
    {
        if( !strncmp(ttyusb_prefixes[i],d->d_name,strlen(ttyusb_prefixes[i])) ) {
            return 1;
        }
        ++i;
    }
    return 0;

    // idk seg faulting here....maybe due to strncmp ? comparing too far? whats the length of tty_usb_prefixes?
    //for (int i = 0; i != sizeof(ttyusb_prefixes)/sizeof(ttyusb_prefixes[0]); ++i) {
}
// find the TTY-usb device (if any) by using <scandir> to search for
// a device with a prefix given by <ttyusb_prefixes> in /dev
// returns:
//  - device name.
// panic's if 0 or more than 1.
//
char *find_ttyusb(void) {
    char *p;
    struct dirent **namelist;
    int n;
    n = scandir("/dev", &namelist, filter, alphasort);
    if (n == -1){
        sys_die(scandir, "scandir failed");
    }
    if (n == 0 || n > 1){
        panic("Either more than 1 or no tty-usb plugged in \n");
    }

    // use <alphasort> in <scandir>
    // return a malloc'd name so doesn't corrupt.

    p = malloc(strlen((*namelist)->d_name) + strlen("/dev/"));
    if (p == NULL) {
        sys_die(malloc, "malloc failed");
    }
    
    strcpy(p,"/dev/");
    strcat(p,(*namelist)->d_name);
    //memcpy(p,(*namelist)->d_name,strlen((*namelist)->d_name));
    output("%s \n",p);
    return p;
}
