#include <unistd.h>
#include <sys/socket.h>

#include "libunix.h"

// fork exec server, create a socket for comparison and dup it to the <to_fd>
int exec_server_socket(int *pid, char **argv, int to_fd) {

    debug("going to fork/exec: %s ", argv[0]);
    for(int i = 1; argv[i]; i++)
        fprintf(stderr, " argv[%d]=<%s> ", i, argv[i]);
    fprintf(stderr, "\n");

    int fds[2];
    if(socketpair(PF_LOCAL, SOCK_STREAM, 0, fds) < 0){
        sys_die(socketpair, failed);
    }

    // argument 0 = cmd-watch
    // argument 1 = name of program
    pid_t child_pid = fork();
    if (child_pid == 0) {
        // child
        if(close(fds[0]) < 0)
            sys_die(close, close failed?);
        if(dup2(fds[1],to_fd) == -1){
            sys_die(dup2, failed);
        }
        execvp(argv[0], argv);
    }

    if(close(fds[1]) < 0)
        sys_die(close, close failed?);

    // set output param
    *pid = child_pid;
    // parent, start watch dog for child
    // technically leaks an fd here but whatever
    start_watchdog(child_pid);

    // return this procs side
    return fds[0];
}
