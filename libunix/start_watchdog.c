#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "libunix.h"

int start_watchdog(pid_t watch_pid) {
  int watch_pipe[2];
  if (pipe(watch_pipe) != 0) {
    sys_die(pipe, pipe got fooked);
  }
  pid_t pid = fork();
  if (pid == -1) {
    sys_die(fork, fork got fooked);
  } else if (pid != 0) {
    // parent
    close(watch_pipe[0]);
    return watch_pipe[1];
  }

  // child
  // detach from parents signals so we can still attempt to blast subordinate proc
  if (setsid() < 0) {
    sys_die(setsid, setsid - failed);
  }
  close(watch_pipe[1]);
  int read_fd = watch_pipe[0];
  size_t res;
  unsigned char c;
  if ((res = read(read_fd, &c, 1)) < 0) {
    sys_die(read, impossible);
  }

  if (res) {
    // parent is taking back responsibility
    exit(0);
  }

  // read returned a 0, so we need to nuke the child
  int kill_res = kill(watch_pid, 9);
  if (kill_res != 0) {
    // if we got a permission error, something is weird.
    demand(errno != EPERM, permission error);
    // the is the only acceptable <errno>
    demand(errno == ESRCH, failed for wrong reason);
  }
  exit(0);
}
