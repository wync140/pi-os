// engler, cs140e: trivial example of how to log GET32.  
//
// part 1: add get32, put32, PUT32.  
//    - you'll have to modify the makefile --- search for GET32 and add there.
//    - simply have put32 call PUT32, get32 call GET32 so its easier to compare output.
//
// part 2: add a simple log for capturing
//
//  without capture it's unsafe to call during UART operations since infinitely
//  recursive and, also, will mess with the UART state.  
//
//  record the calls in a log (just declare a statically sized array until we get malloc)
//  for later emit by trace_dump()
//
// part 3: keep adding more functions!  
//      often it's useful to trace at a higher level, for example, doing 
//      uart_get() vs all the put's/get's it does, since that is easier for
//      a person to descipher.  or, you can do both:
//          -  put a UART_PUTC_START in the log.
//          - record the put/gets as normal.
//          - put a UART_PUTC_STOP in the log.
//  
// XXX: may have to use wdiff or similar to match the outputs up exactly.  or strip out
// spaces.
//

#include "rpi.h"
#include "trace.h"

// gross that these are not auto-consistent with GET32 in rpi.h
unsigned __wrap_GET32(unsigned addr);
unsigned __real_GET32(unsigned addr);
unsigned __real_get32(const volatile void *addr);
unsigned __wrap_get32(const volatile void *addr);

void __wrap_PUT32(unsigned addr, unsigned v);
void __real_PUT32(unsigned addr, unsigned v);

void __real_put32(volatile void *addr, unsigned v);
void __wrap_put32(volatile void *addr, unsigned v);

typedef enum {
    PUT = 0,
    GET = 1
} mem_direction;

struct mem_t{
    mem_direction dir; // is it a put or a get? 0: put 1: get; 
    unsigned addr;
    unsigned v;
};

// static array for recording captures
#define capture_backing_size 256
static struct mem_t capture_backing[capture_backing_size];
static int capture_backing_idx = 0; // holds where we should write the next write

static int tracing_p = 0;
static int in_trace = 0;
static int capturing_p = 0;

// take a valid mem_t and reocrd into the backing
void log_mem_t(struct mem_t node){
    capture_backing[capture_backing_idx] = node;
    capture_backing_idx = capture_backing_idx + 1;
    demand(capture_backing_idx <= capture_backing_size,"Outstripped size of capture backing in trace.c");
}

void print_mem_t(struct mem_t node){
    if(node.dir == PUT){
        printk("\tTRACE:PUT32(0x%x)=0x%x\n", node.addr, node.v);
    }else{
        printk("\tTRACE:GET32(0x%x)=0x%x\n", node.addr, node.v);
    }
}


void trace_start(int capture_p) {
    if(capture_p){
        capturing_p = 1;
    }else{
        capturing_p = 0;
    }
    demand(!tracing_p, "trace already running!");
    demand(!in_trace, "invalid in_trace");
    tracing_p = 1; 
}

// the linker will change all calls to GET32 to call __wrap_GET32
unsigned __wrap_GET32(unsigned addr) { 
    // the linker will change the name of GET32 to __real_GET32,
    // which we can then call ourselves.
    unsigned v = __real_GET32(addr);
    if(!in_trace && tracing_p) {
        in_trace = 1;
        // doing this print while the UART is busying printing a character
        // will lead to an inf loop since printk will do its own
        // puts/gets.  use <in_trace> to skip our own monitoring calls.
        if(capturing_p){
            struct mem_t mem = {1,addr,v};
            log_mem_t( mem);
        } else {
                // match it up with your unix print so you can compare.
                // we have to add a 0x
                printk("\tTRACE:GET32(0x%x)=0x%x\n", addr, v);
        }
        in_trace = 0;
    }
    return v;
}

unsigned __wrap_get32(const volatile void *addr){
    return __wrap_GET32((unsigned)addr);
}

void __wrap_PUT32(unsigned addr, unsigned v){
    __real_PUT32(addr,v);
    
    if(!in_trace && tracing_p) {
        in_trace = 1;

        if(capturing_p){
            struct mem_t mem = {0,addr,v};
            log_mem_t( mem );
        } else {
                printk("\tTRACE:PUT32(0x%x)=0x%x\n", addr, v);
                
        }

        in_trace = 0;
    }
}

void __wrap_put32(volatile void *addr, unsigned v){
    __wrap_PUT32((unsigned)addr,v);
}


void trace_stop(void) {
    demand(tracing_p, "trace already stopped!\n");
    tracing_p = 0; 
}

void trace_dump(int reset_p) { 
    demand(capturing_p, "Need to be in capture mode");

    for(int i = 0; i != capture_backing_idx; i++){
        print_mem_t(capture_backing[i]);
    }

    if (reset_p == 0){
        capture_backing_idx = 0;
    }

}
