#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "libunix.h"

int main(int argc, char *argv[]) {
   	pid_t pid = fork();
    if (pid <0 ){
        sys_die(fork, fork-failed);
    }
    if (pid != 0){
        //parent
        start_watchdog(pid);
        printf("parent waiting to be killed\n");
        //sleep(3);
        printf("parent about to die\n");
        while(1){
            (void)0;
        }
        exit(0);
    }
    else {
        // child proc, this one is the one that will be nuked by start_watchdog
        printf("Child proc starting, pid is %d\n",pid);
        if(setsid() < 0){
            sys_die(setsid, setsid-failed);
        }
        while(1){
            (void)0;
        };
        exit(1);
    }
    

}
