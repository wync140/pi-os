#include "rpi.h"
#include <string.h>
#include "libc/crc.h"

#define ARMBASE 0x8000
#define SIZE_LOCATION 0x8004
#define CRC_LOCATION 0x8008
#define CODE_LOCATION 0x800C
#define NOOPHEX 0xe1a00000

void notmain(void) {
    uart_init();
    uint32_t size;
    uint32_t crc;
    memcpy(&size,(void*)SIZE_LOCATION,sizeof(uint32_t));
    memcpy(&crc,(void*)CRC_LOCATION,sizeof(uint32_t));

    printk("size was read as: %d\n",size);
    printk("crc was read as:  %d\n\n",crc);

    if(size == NOOPHEX){
        printk("Error, size was the no op code, did you place the size correctly?\n");
        printk("DONE!!!\n");
        rpi_reboot();
    }
    if(crc == NOOPHEX){
        printk("Error, crc was the no op code, did you place the size correctly?\n");
        printk("DONE!!!\n");
        rpi_reboot();
    }
    uint8_t* program_start = (uint8_t*)CODE_LOCATION;

    // lets check CRC
    uint32_t calc_crc = our_crc32(program_start,size);
    printk("calculated crc was: %d\n", calc_crc);

    if(calc_crc != crc){
        printk("ERROR: Crcs didn't match \n");
    }else{
        printk("Crcs matched! \n");
    }
    printk("DONE!!!\n");
    rpi_reboot();
}
