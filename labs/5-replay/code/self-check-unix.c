// simple driver for replaying a trace.
// example:
//   ./replay ./log.interpose.txt my-install ./hello.bin
//
// first get the successful run working.  then do the corruption by removing the
// #if 0
//
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "interpose.h"
#include "libunix.h"
#include <signal.h>
#include <string.h>
#include <unistd.h>

void usage(void) {
  output("Usage error: `self-check-unix <binary> <output>\n");
  exit(1);
}

int main(int argc, char *argv[]) {
  if (argc < 3) {
    output("too few arguments: %d\n", argc);
    usage();
  }

  uint32_t nbytes;
  size_t wordSz = sizeof(uint32_t);
  output("Word Size is:    %ld\n",wordSz);
  size_t offset = 3 * wordSz;
  uint8_t *code = read_file(&nbytes, argv[1]);
  output("code size is: %d\n",nbytes);
  uint32_t codeSize = nbytes - offset;
  uint32_t crc = our_crc32(&code[offset],codeSize);

  output("First byte of crc: %d\n",code[offset]);
  output("Last byte of crc:  %d\n",code[offset+codeSize-1]);

  output("Stamped size is: %d\n",codeSize);
  output("Stamped crc is:  %d\n",crc);
  
  int fd = open(argv[2], O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
  if( fd < 0){
    die("failed to open");
  }
  // re construct, write firs 4 bytes
  if(write(fd,code,wordSz) < wordSz){
    die("write_failed");
  }
  // now write size
  if(write(fd,(char*)(&codeSize), wordSz) < wordSz){
    die("write_failed");
  }

  // now write crc
  if(write(fd,(char*)(&crc), wordSz) < wordSz){
    die("write_failed");
  }
  // now write rest of binary 
  if(write(fd,&(code[offset]), codeSize) < codeSize){
    die("write_failed");
  }
  output("done writing binary with crc\n");
  
}
