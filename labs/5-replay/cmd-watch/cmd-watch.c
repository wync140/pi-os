// simple program to run a command when any file that is "interesting" in a
// directory changes. e.g.,
//      cmd-watch make
// will run make at each change.
//
// This should use the scandir similar to how you did `find_ttyusb`
//
// key part will be implementing two small helper functions (useful-examples/
// will be helpful):
//  - static int pid_clean_exit(int pid);
//  - static void run(char *argv[]);
//
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "libunix.h"

#define _SVID_SOURCE
#include <dirent.h>

// 1 if should be tracked
int is_tracked(char **suffixes, size_t suffix_length, char *entry) {
  size_t d_name_len = strlen(entry);
  for (size_t i = 0; i != suffix_length; i++) {
    // where we start the str cmp
    int32_t offset = d_name_len - strlen(suffixes[i]);
    if (offset < 0) {
      // strlen suffixes would create an invalid ptr, skip this one
      break;
    }
    char *start_suffix = entry + (size_t)offset;
    int ret = strncmp(start_suffix, suffixes[i], strlen(suffixes[i]));
    if (ret == 0) {
      // cool it matched
      return 1;
    }
  }
  // didn't find any
  return 0;
}

// scan the files in "./" (you can extend this) for those
// that match the suffixes in <suffixes> and check  if any
// have changed since the last time.
// ret 0 if no new changes since last_time, will modify last_time to new time
int check_activity(time_t *last_time) {
  char *suffixes[] = {".c", ".h", ".S", "Makefile"};
  size_t suffix_len = sizeof(suffixes) / sizeof(suffixes[0]);
  const char *dirname = ".";
  int changed_p = 0;

  DIR *cwd = opendir(dirname);
  if (cwd == NULL) {
    sys_die(opendir, failed to open the directory);
  }
  time_t most_recent_time = *last_time;
  errno = 0;
  struct dirent *entry = NULL;
  while ((entry = readdir(cwd))) {
    if (errno != 0) {
      sys_die(readdir, eerno set);
      // just return that we don't know, don't nuke yourself
      return 0;
    }

    // skip this one if we aren't tracking it
    if (!is_tracked(suffixes, suffix_len, entry->d_name)) {
      break;
    }

    // ok we are tracking, inspect.
    struct stat info;
    int res = stat(entry->d_name, &info);
    if (res != 0) {
      sys_die(stat, stat failed);
    }

    // check our mod time, is it more
    if (info.st_mtime > most_recent_time) {
      most_recent_time = info.st_mtime;
    }
  }

  if (closedir(cwd)) {
    sys_die(closedir, failed to close);
  }

  // return new most_recent_time
  if (most_recent_time <= *last_time) {
    return 0;
  }

  int ret_code = 1;

  // initializing
  if (*last_time == 0) {
    ret_code = 0;
  }
  // ok newer time
  *last_time = most_recent_time;
  return ret_code;
}

// synchronously wait for <pid> to exit.  returns 1 if it exited
// cleanly (via exit(0)), 0 otherwise.
static int pid_clean_exit(pid_t pid) {
  int status;
  if (waitpid(pid, &status, 0) < 0) {
    sys_die(waitpid, should never fail);
  }

  if (!WIFEXITED(status)) {
    return 0;
  }

  return 1;
}

// simple helper to print null terminated vector of strings.
static void print_argv(char *argv[]) {
  assert(argv[0]);

  fprintf(stderr, "about to exec =<%s ", argv[0]);
  for (int i = 1; argv[i]; i++)
    fprintf(stderr, " %s", argv[i]);
  fprintf(stderr, ">\n");
}

// fork/exec <argv> and wait for the result: print an error
// and exit if the kid crashed or exited with an error (a non-zero
// exit code).
static void run(char *argv[]) {

  // argument 0 = cmd-watch
  // argument 1 = name of program
  pid_t pid = fork();
  if (pid == 0) {
    // child
    execvp(argv[1], &(argv[1]));
  }
  // parent waits.
  int exited_clean = pid_clean_exit(pid);
  if (!exited_clean) {
    output("child process died, killing this one.");
    exit(1);
  }
}

int main(int argc, char *argv[]) {
  if (argc < 2)
    die("cmd-watch: not enough arguments\n");

  time_t latest_mod = 0;
  while (1) {
    usleep(250 * 1000);
    if (check_activity(&latest_mod)) {

      printf("Running watched cmd modification!\n");
      run(argv);
    }
  }

  return 0;
}
