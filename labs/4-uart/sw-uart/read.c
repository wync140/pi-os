// the sw uart should  print hello world 10x if it works.
#include "rpi.h"
#include "sw-uart.h"

#include "cycle-util.h"


void notmain(void) {
    // strictly speaking: don't need this.
    uart_init();

    // use pin 20 for tx, 21 for rx
    sw_uart_t u = sw_uart_init(20,21, 115200);
    char exit = 'x';
    int ret = 0;

    while (ret != exit){

        ret = sw_uart_getc(&u,5000000);
        if(ret == -1){
            //sw_uart_putk(&u,"returned -1\n");
            printk("returned -1\n");
        }
        else{
            //sw_uart_putk(&u,str);
            printk("Got this char: <%c>\n",(char)ret);
        }
    }

    clean_reboot();

}
