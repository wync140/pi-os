#include "rpi.h"
#include "sw-uart.h"
#include "cycle-util.h"

#define CYCLES_PER_US 700

// helper: cleans up the code.
static inline void timed_write(int pin, int v, unsigned usec) {
    gpio_write(pin,v);
    delay_us(usec);
}

// do this first: used timed_write to cleanup.
//  recall: time to write each bit (0 or 1) is in <uart->usec_per_bit>
void sw_uart_putc(sw_uart_t *uart, unsigned char c) {

    cycle_cnt_init();
    
    // can add this here to double baud rate but this feels like a heavy hammer that should be decided 
    // in a uart function
    //enable_cache()

    const unsigned cycles_to_wait =  CYCLES_PER_US * uart->usec_per_bit;

    gpio_set_output(uart->tx);

    // dynamically time the gpio_write
    const unsigned write_counts = TIME_CYC(gpio_write(uart->tx,1));
    delay_ncycles(cycle_cnt_read(),cycles_to_wait);

    // add one GPIO write to the start to permentantly offset cycle 
    unsigned end = cycles_to_wait + write_counts;
    const unsigned start = cycle_cnt_read();

    // start bit
    write_cyc_until(uart->tx,0,start,end); 
    end += cycles_to_wait;

    // data bits
    unsigned int mask = 1;
    for(int i = 0; i != 8; i++){
        write_cyc_until(uart->tx, c & mask, start, end);
        end += cycles_to_wait;
        mask = mask << 1;
    }

    // stop bit
    write_cyc_until(uart->tx,1,start,end);
}

// do this second: you can type in pi-cat to send stuff.
//      EASY BUG: if you are reading input, but you do not get here in 
//      time it will disappear.
int sw_uart_getc(sw_uart_t *uart, int timeout_usec) {

    cycle_cnt_init();
    
    gpio_set_input(uart->rx);

    const unsigned read_counts = TIME_CYC(gpio_read(uart->rx));

    const unsigned cycles_per_bit =  CYCLES_PER_US * uart->usec_per_bit;
    
    if(read_counts > cycles_per_bit){
        printk("Read counts too slow at: %s:%d",__FILE__,__LINE__);
        return -1;
    }

    const unsigned total_cycles_to_timeout = CYCLES_PER_US * timeout_usec;

    if(total_cycles_to_timeout < timeout_usec){
        printk("timeout usec specified too long, overflowed in %s:%d",__FILE__,__LINE__);
        return -1;
    }

    const unsigned timeout_start = cycle_cnt_read();

    // spin on this, if this escapes we will return -1
    while((cycle_cnt_read() - timeout_start) < total_cycles_to_timeout) {

        if(gpio_read(uart->rx) == 0){
            int ret = 0;
            // start bit detected
            // move forward .5 cycle, plus a time to gpio_read to permenantly offset forward slightly,
            // and then step forward one cycle. Now we should be aligned at the first data bit after waiting
            unsigned end = cycles_per_bit / 2 + cycles_per_bit + read_counts;
            unsigned start = cycle_cnt_read();

            // delay to that time and then start reading
            delay_ncycles(start,end);

            for(int i = 0; i != 8; i++){
                ret |= gpio_read(uart->rx) << i;
                end += cycles_per_bit;
                delay_ncycles(start,end);
            }

            return ret;
        }

    }
    // escaped while loop, return -1;
    return -1;

}

void sw_uart_putk(sw_uart_t *uart, const char *msg) {
    for(; *msg; msg++)
        sw_uart_putc(uart, *msg);
}

