// engler: simple example of writing pins for a given number of cycles.
#include "rpi.h"
#include "cycle-util.h"

void notmain(void) {
    int tx = 20;
    int rx = 21;
    gpio_set_output(tx);
    gpio_set_input(rx);
    gpio_write(tx,0);

    cycle_cnt_init();

    // seems to help: try with and without.
    enable_cache();

    // pick some arbitrary number of cycles to write (not too small unless
    // you're going to get fancy.)
    unsigned n = 7777;
    printk("about to start sending bits: should delay <%d>cyc\n", n);

    // note: if you look at the assembly of the code below, you'll see gcc is
    // extremely stupid about large contants.  rather than putting in a register
    // it keeps loading it from memory.  given that we run uncached, this is slow.
    // fixing this (i had to pass the values as arguments to a function in another
    // file) is kinda annoying, but does reduce variance.  there is probably
    // a better gcc-specific way.
    unsigned end = n;
    unsigned start = cycle_cnt_read();

    write_cyc_until(tx, 1, start, end);  end += n;   // 1
    write_cyc_until(tx, 0, start, end);  end += n;   // 2
    write_cyc_until(tx, 1, start, end);  end += n;   // 3
    write_cyc_until(tx, 0, start, end);  end += n;   // 4
    write_cyc_until(tx, 1, start, end);  end += n;   // 5
    write_cyc_until(tx, 0, start, end);  end += n;   // 6
    write_cyc_until(tx, 1, start, end);  end += n;   // 7
    write_cyc_until(tx, 0, start, end);  end += n;   // 8
    write_cyc_until(tx, 1, start, end);  end += n;   // 9
    write_cyc_until(tx, 0, start, end);  end += n;   // 10

    printk("start: <%u>cyc\n", start);
    printk("end: <%u>cyc\n", end);
    printk("\r\n");

    // cycle cnt read is fairly unaffected by cyclic rate/cache
    TIME_CYC_PRINT10("Time for diff",cycle_cnt_read());
    TIME_CYC_PRINT("Time for diff",cycle_cnt_read());

    TIME_CYC_PRINT10("gpio_write(pin,v)", gpio_write(tx,0));
    TIME_CYC_PRINT("gpio_write(pin,v)", gpio_write(tx,0));

    TIME_CYC_PRINT10("gpio_read(rx)", gpio_read(rx));
    TIME_CYC_PRINT("gpio_read(rx)", gpio_read(rx));

    TIME_CYC_PRINT("Time for 1us delay",delay_us(1));

    TIME_CYC_PRINT("timer_get_usec()",timer_get_usec());

    TIME_CYC_PRINT("bitwiseops()",{ unsigned int yes = 2; yes = yes<<1; yes|yes; });


    uint32_t start_time = timer_get_usec();
    // 70k cycles should be roughly 100us;
    delay_ncycles(cycle_cnt_read(),70000);
    uint32_t end_time = timer_get_usec();
    printk("start_time:<%u>\n",start_time);
    printk("end_time:<%u>\n",end_time);
    printk("time for a lot of cycless: <%u>us\n", end_time-start_time);
    
    printk("done\n");
    clean_reboot();
}
