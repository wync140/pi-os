#include "gpio.h"

// writes the 32-bit value <v> to address <addr>:   *(unsigned *)addr = v;
void put32(volatile void *addr, unsigned v);
// returns the 32-bit value at <addr>:  return *(unsigned *)addr
unsigned get32(const volatile void *addr);

#define BASE_GPIO_ADDR 0x20200000

void putter(){
    volatile unsigned *gpio_fsel2 = (void*) (BASE_GPIO_ADDR + 0x08);
    volatile unsigned *gpio_levl0 = (void*) (BASE_GPIO_ADDR + 0x34);
    put32(gpio_fsel2,1);
    put32(gpio_fsel2,0);
    int b = (get32(gpio_levl0) | (1 << 20));
    b++;
}