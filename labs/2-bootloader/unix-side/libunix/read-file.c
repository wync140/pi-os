#include <assert.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <demand.h>
#include <errno.h>

#include "libunix.h"

// read entire file into buffer.  return it.   zero pads to a 
// multiple of 4.
//
// make sure to cleanup!
uint8_t *read_file(unsigned *size, const char *name) {
    uint8_t *buf = 0;
    struct stat st;
    int ret = stat(name,&st);
    if (ret != 0){
        sys_die(stat, "read_file failed");
    }
    demand(st.st_size<100000000, "File size is greater than 100Mb. Refusing to load into mem");

    int fd  = open(name, O_RDONLY);
    if (fd == -1){
        sys_die(open, "open failed");
    }

    size_t padding_multiple = 4;
    size_t padded_size = (st.st_size + padding_multiple - 1) / padding_multiple * padding_multiple;

    buf = calloc(padded_size,sizeof(uint8_t));
    if (buf == NULL){
        sys_die(calloc, "calloc failed");
    }

    int n = read (fd,buf,st.st_size);
    if (n == -1){
        sys_die(read, "read failed");
    }
    // now my read is failing...

    int closed  = close(fd);
    if (closed != 0){
        sys_die(close, "close failed");
    }
    *size = padded_size;
    return buf;
}
