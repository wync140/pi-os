/*
 * implement these.  ordered in difficulty.
 */
#include "rpi-asm.h"

@ return the current stack pointer.
MK_FN(rpi_get_sp)
    mov r0, sp
	bx lr

@empty stub routines.  use these, or make your own.
MK_FN(rpi_cswitch)

    @ save current context into the buffer
    @ pointed to by r0
    @ this is a dsb
    mcr p15, 0, r0, c7, c10, 4
    stmea r0, {r4-r12,sp,lr}

    @ warp r1 forward the number of regs we saveh
    add r1, #44
    ldmea r1, {r4-r12,sp,lr}
    mcr p15, 0, r0, c7, c10, 4 
    bx lr

@ [Make sure you can answer: why do we need to do this?]
@
@ use this to setup each thread for the first time.
@ setup the stack so that when cswitch runs it will:
@	- load address of <rpi_init_trampoline> into LR
@	- <code> into r1, 
@	- <arg> into r0
@ 
MK_FN(rpi_init_trampoline)
    @ because of initialization we know r4 has our thread ptr, so we move this to r0
    @ and then call the c trampoline
    mov r0, r4
    bl rpi_thread_c_trampoline
    @ we should never return here
    bl abort_proc
    
