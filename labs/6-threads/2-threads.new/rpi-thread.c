// engler, cs140e: starter code for trivial threads package.
#include "rpi.h"
#include "rpi-thread.h"

// typedef rpi_thread_t E;
#define E rpi_thread_t
#include "Q.h"

// currently only have a single run queue and a free queue.
static Q_t runq, freeq;

static rpi_thread_t *cur_thread;        // current running thread.
static rpi_thread_t *scheduler_thread;  // first scheduler thread.
static uint32_t register_dump_area[16]; // for a safe spot to dump registers during exit

// monotonically increasing thread id.
static unsigned tid = 1;
// total number of blocks we have allocated.
static unsigned nalloced = 0;

// return pointer to the current thread.  
rpi_thread_t *rpi_cur_thread(void) {
    return cur_thread;
}

#define is_aligned(_p,_n) (((unsigned)(_p))%(_n) == 0)

void rpi_stack_check_t(rpi_thread_t *t_itr);

// keep a cache of freed thread blocks.  call kmalloc if run out.
static rpi_thread_t *th_alloc(void) {
    rpi_thread_t *t = Q_pop(&freeq);

    // stack has to be 8-byte aligned.
    if(!t) {
        t = kmalloc_aligned(sizeof *t, 8);
        nalloced++;
    }
    demand(is_aligned(&t->stack[0],8), stack must be 8-byte aligned!);
    t->tid = tid++;
    return t;
}

static void th_free(rpi_thread_t *th) {
    // push on the front in case helps with caching.
    Q_push(&freeq, th);
}

/***********************************************************************
 * implement the code below.
 */
#define GUARD_WORD (0xDEADBEEF)
// create a new thread.
rpi_thread_t *rpi_fork(void (*code)(void *arg), void *arg) {
    rpi_thread_t *t = th_alloc();

    /*
     * do the brain-surgery on the new thread stack here.
     * must set up initial stack: 
     *  - set LR, SP, and store <code> and <arg> where trampoline can get it.
     */

    // set up lr, sp and set r4 to our ptr so we can jump to the c trampoline
    t->reg_save_area[0] = (uint32_t)t;
    t->reg_save_area[REG_SP_OFF] = (uint32_t)&(t->stack[THREAD_MAXSTACK]);
    t->reg_save_area[REG_LR_OFF] = (uint32_t)rpi_init_trampoline;
    for(size_t i = 0; i!= STACK_GUARD_SZ; i++){
        t->stack_guard[i] = GUARD_WORD;
    }
    t->fn = code;
    t->arg = arg;

    Q_append(&runq, t);
    return t;
}

void rpi_thread_c_trampoline(rpi_thread_t* thread) {
    
    thread->fn(thread->arg);
    // ideally shouldn't return but we call exit if it does in case
    //printk("Thread %d didn't call exit\n",thread->tid);
    rpi_exit(0);
}

// exit current thread.
void rpi_exit(int exitcode) {
	/*
	 * 1. free current thread.
	 *
	 * 2. if there are more threads, dequeue one and context
 	 * switch to it.
	 
	 * 3. otherwise we are done, switch to the scheduler thread 
	 * so we call back into the client code.
	 */
    rpi_internal_check();
    
    th_free(cur_thread);
    if(!Q_empty(&runq)) {
        cur_thread = Q_pop(&runq);
        // later!
        rpi_cswitch(register_dump_area,cur_thread->reg_save_area); 
    }

    // q was empty, so lets flip back to scheduler
    rpi_cswitch(register_dump_area,scheduler_thread->reg_save_area); 
}

// yield the current thread.
void rpi_yield(void) {
	// if cannot dequeue a thread from runq
	//	- there are no more runnable threads, return.  
	// otherwise: 
	//	1. put current thread on runq.
	// 	2. context switch to the new thread.
    //printk("trying to yield\n");
    if(Q_empty(&runq)) {
        return;
    }

    rpi_thread_t* this_thread = cur_thread;
    Q_append(&runq,this_thread);

    // reset cur_thread
    cur_thread = Q_pop(&runq);
    rpi_cswitch(this_thread->reg_save_area,cur_thread->reg_save_area);
    // now this thread's lr is here. when we eventually come back to this
    // thread it will return to users code here. 
}

void assert_thread_start(void* arg){
    (void)arg;
    demand(0,"this should have never ran!!\n");
}

void abort_proc(void){
    demand(0,"The thread should have never reached here");
}

/*
 * starts the thread system.  
 * note: our caller is not a thread!  so you have to figure
 * out how to handle this.
 */
void rpi_thread_start(void) {
    // statically check that the stack is 8 byte aligned.
    AssertNow(offsetof(rpi_thread_t, stack) % 8 == 0);
    // statically check that the register save area is at offset 0.
    AssertNow(offsetof(rpi_thread_t, reg_save_area) == 0);

    // no other runnable thread: return.
    if(Q_empty(&runq)) {
        printk("rpithreads, no threads loaded so returning immediately\n");
        return;
    }

    rpi_internal_check();
    printk("thread: internal check passed\n");

    //  1. create a new fake thread 
    //  2. dequeue a thread from the runq
    //  3. context switch to it, saving current state in
    //	    <scheduler_thread>
    scheduler_thread = th_alloc();
    scheduler_thread->fn = assert_thread_start;
    scheduler_thread->arg = 0;

    // reset scheduler thread tid to 0;
    scheduler_thread->tid = 0;

    cur_thread = Q_pop(&runq);
    
    // we are about to jump into this thread, so its the cur thread. 
    rpi_cswitch(scheduler_thread->reg_save_area,cur_thread->reg_save_area);

    // now current thread is scheduler_thread
    cur_thread = scheduler_thread;

    // lr of scheduler_thread is right here, when we eventually CTX switch back
    // to this supervisor thread we will be here
    printk("rpithreads: done with all threads! returning\n");
}


// internal consistency checks.
void rpi_internal_check(void) {
    // the blocks on the runq + freeq must equal the total ever allocated
    // or we have leaked storage.
    unsigned n_free = Q_nelem(&freeq),
             n_run = Q_nelem(&runq),
             n = n_free + n_run;

    if(nalloced != n)
        panic("storage leak: should have %d free blocks, have %d (runq=%d, freeq=%d)\n", 
            nalloced, n, n_free,n_run);

    // active thread system: 
    //      walk through the run queue making sure the stack pointer
    //     and ra pointer makes sense.
    if(!Q_empty(&runq)) {
        rpi_thread_t *t_itr =  Q_start(&runq);
        while(t_itr != NULL){
            rpi_stack_check_t(t_itr);
            t_itr = Q_next(t_itr);
        }
    }
}

// check the threads stack
void rpi_stack_check_t(rpi_thread_t *t_itr) {
    demand(t_itr->reg_save_area[REG_SP_OFF] > (uintptr_t)(&(t_itr->stack[0])),"Stack horked!");
    demand(t_itr->reg_save_area[REG_SP_OFF] <= (uintptr_t)(&(t_itr->stack[THREAD_MAXSTACK])),"Stack horked!");
    for(size_t i = 0; i!= STACK_GUARD_SZ; i++){
        demand(t_itr->stack_guard[i] == GUARD_WORD,"stack horked!");
    }
}

void rpi_stack_check(void) {
    rpi_stack_check_t(cur_thread);
}

/********************************************************************
 * save for homework.
 */

// block caller until thread <th> completes.
void rpi_join(rpi_thread_t *th) {
    unimplemented();
}

// put the current thread on a blocked queue: resume in 
// exactly // <usec> micro-seconds.  not before, not after.
// should give low-error resumption times.
void rpi_exact_sleep(uint32_t usec) {
    unimplemented();
}

// change so that it prints out which registers are sp and lr.
void print_reg_save(uint32_t *regs) {
    demand(REG_SP_OFF != 0, you should fix this value!);
    demand(REG_LR_OFF != 0, you should fix this value!);

    unsigned off = 0;
    for(int r = 4; r <= 12; r++, off++){
        printk("offset=%d, r%d = 0x%x [%d]\n",  off, r, regs[off],regs[off]);
    }
    printk("offset=%d, r13/sp = 0x%x [%d]\n", REG_SP_OFF, regs[REG_SP_OFF],regs[REG_SP_OFF]);
    printk("stack value should be around 0x%x, is 0x%x\n", &off, regs[REG_SP_OFF]);
    printk("offset=%d, r14/lr = 0x%x [%d]\n", REG_LR_OFF, regs[REG_LR_OFF],regs[REG_LR_OFF]);
}

void print_and_die(uint32_t *save) {
    printk("in print and die: about to dump registers from %x\n", save);
    printk("each register value except <sp> should hold its reg number (r4 holds 4, etc)\n");
    print_reg_save(save);
    clean_reboot();
}
