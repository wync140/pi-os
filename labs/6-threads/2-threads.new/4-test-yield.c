#include "rpi.h"
#include "rpi-thread.h"

// test that we can yield and do a simple realtime thing.
struct pwm {
    unsigned duty;  // between 0 and 100
    unsigned pin;
};

// wait for <n> from the current time.  we yield
// until enough time passes.  if yield is fast and
// other threads don't run too long, this can be
// accurate real-time, even though we don't have 
// an actual real-time scheduler.
static void wait_usec(unsigned n) {
    //printk("wait time: %u\n",n);
    demand(n < 100000, "unlikely large delay = %dusec!\n", n);
    unsigned start = timer_get_usec();
    while(1) {
        unsigned current_time = timer_get_usec();
        if((current_time - start) > n){
            //printk("current_time, start_time: %u, %u\n",current_time, start);
            return;
        }
        //printk("going to yeild: %d\n",rpi_cur_thread()->tid);
        rpi_yield();
    }
}

// if our threads are fast enough you can run a bunch of these
// and not see weird flickers.
void blink(void *arg) {
    struct pwm *p = arg;
    printk("thread: %d\n",rpi_cur_thread()->tid);
    printk("current sp is: 0x%x\n",rpi_get_sp());


    gpio_set_output(p->pin);
    demand(p->duty > 0 && p->duty <= 100, wierd duty cycle!);
    
    // how long we should be on out of 100 usec.
    unsigned on_usec = p->duty;
    unsigned off_usec = 100 - p->duty;

    for(int i = 0; i < 10000; i++) {
        gpio_set_on(p->pin);
        wait_usec(on_usec);
        gpio_set_off(p->pin);
        wait_usec(off_usec);
    }
    rpi_exit(0);
    
}

void notmain(void) {
    uart_init();
    kmalloc_init();

    struct pwm t_75 = {.duty = 90, .pin = 20 },
               t_25 = {.duty = 5, .pin = 21 };

    rpi_fork(blink, &t_75);
    rpi_fork(blink, &t_25);
    rpi_thread_start();
    printk("done\n");
    clean_reboot();
}
