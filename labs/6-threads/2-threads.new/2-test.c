/*
 * second test: simple context switching test.
 */

#include "rpi.h"
#include "rpi-thread.h"

/*
 * write these routines in 2-asm.S
 */
// *u = <x> in 2-asm.S
void store_one_asm(unsigned *u, unsigned x);
void check_callee_save(uint32_t *save);
void cswitch(uint32_t *old, uint32_t *new);


void notmain() {
    uart_init();
    kmalloc_init();

    /*
     * part 0
     */
    printk("-----------------------------------------------------\n");
    printk("part 0: write a value into a pointer in assembly\n");
    unsigned u;
    store_one_asm(&u, 12);
    printk("u=%d\n", u);
    assert(u==12);
    store_one_asm(&u, 0xfeedface);
    assert(u==0xfeedface);
    printk("part0: SUCCESS!\n");

    printk("-----------------------------------------------------\n");
    // sizeof save area
    unsigned nbytes = sizeof ((rpi_thread_t*)0)->reg_save_area;
    uint32_t *save = kmalloc_aligned(nbytes, 8);

    /*
     * part 1: check that you can save callee registers with known values.
     * prints and dies.
     *
     * comment this out to do the next one.
     */
    printk("part1: about to test register saving\n");
    printk("before asm routine %x\n", save);
    check_callee_save(save);
    panic("Should not return\n");
	clean_reboot();
}
