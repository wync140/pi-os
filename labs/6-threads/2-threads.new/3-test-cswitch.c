/*
 * second test: simple context switching test.
 */

#include "rpi.h"
#include "rpi-thread.h"

void cswitch(uint32_t *old, uint32_t *new);

void notmain() {
    uart_init();
    kmalloc_init();

    // sizeof save area
    unsigned nbytes = sizeof ((rpi_thread_t*)0)->reg_save_area;
    uint32_t *save = kmalloc_aligned(nbytes, 8);

    /*
     * part 1: check that we can save/restore the same state.
     */
    int i;
    printk("\n\n-----------------------------------------\n");
    printk("Part1: about to cswitch(old,old)\n");
    cswitch(save,save);
    printk("worked: gonna do alot of cswitches\n");
    for(i = 0; i < 100; i++)
        cswitch(save,save);
    printk("Part1: SUCCESS\n");

    /*
     * Part 2: kinda brain-twisting, we use <cswitch> to checkpoint
     * a set of register values.  We roll back to that execution point
     * by restoring to this checkpoint at a later date.  This should
     * "just work" if your code works.
     */
    printk("\n\n-----------------------------------------\n");
    printk("Part 2: gonna do time travel\n");
    // keep it out of a register
    volatile int *count = kmalloc(sizeof *count); 
    *count = 0;

    uint32_t *ckpt = kmalloc_aligned(nbytes, 8);
    
    printk("before cswitch(ckpt)\n");
    cswitch(ckpt,ckpt);
    printk("after cswitch(ckpt)\n");

    *count += 1;
    if(*count >= 4) {
        printk("done!  count=%d\n", *count);
        assert(*count == 4);
        printk("Part2: SUCCESS!\n");
        clean_reboot();
    }
    printk("going to time travel: cnt = %d\n", *count);
    cswitch(save, ckpt);

    panic("IMPOSSIBLE: should not reach this\n");
	clean_reboot();
}


// change so that it prints out which registers are sp and lr.
void print_reg_save(uint32_t *regs) {
    demand(REG_SP_OFF != 0, you should fix this value!);
    demand(REG_LR_OFF != 0, you should fix this value!);

    unsigned off = 0;
    for(int r = 4; r <= 12; r++, off++){
        printk("offset=%d, r%d = 0x%x [%d]\n",  off, r, regs[off],regs[off]);
    }
    printk("offset=%d, r13/sp = 0x%x [%d]\n", REG_SP_OFF, regs[REG_SP_OFF],regs[REG_SP_OFF]);
    printk("stack value should be around 0x%x, is 0x%x\n", &off, regs[REG_SP_OFF]);
    printk("offset=%d, r14/lr = 0x%x [%d]\n", REG_LR_OFF, regs[REG_LR_OFF],regs[REG_LR_OFF]);
}

void print_and_die(uint32_t *save) {
    printk("in print and die: about to dump registers from %x\n", save);
    printk("each register value except <sp> should hold its reg number (r4 holds 4, etc)\n");
    print_reg_save(save);
    clean_reboot();
}

void print_last_byte(uint32_t *r0, uint32_t *r1){
    printk("last byte from asm: %p\n",r1);
    clean_reboot();
}

void print_word_and_die(uint32_t r0){
    printk("word given: 0x%x\n",r0);
    clean_reboot();
}
